<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

/** These settings are automatically used on your OpenShift Gear*/
if (getenv('OPENSHIFT_APP_NAME') != "") {
	/** The name of the database for WordPress */
	define('DB_NAME', getenv('OPENSHIFT_APP_NAME'));

	/** MySQL database username */
	define('DB_USER', getenv('OPENSHIFT_MYSQL_DB_USERNAME'));

	/** MySQL database password */
	define('DB_PASSWORD', getenv('OPENSHIFT_MYSQL_DB_PASSWORD'));

	/** MySQL hostname */
	define('DB_HOST', getenv('OPENSHIFT_MYSQL_DB_HOST') . ':' . getenv('OPENSHIFT_MYSQL_DB_PORT'));

/** These settings can be configured for your local development environment
	and will not affect your OpenShift configuration */
} else {
	define('DB_NAME', 'jmbonilla');

	/** MySQL database username */
	define('DB_USER', 'root');

	/** MySQL database password */
	define('DB_PASSWORD', 'root');

	/** MySQL hostname */
	define('DB_HOST', 'localhost');
}



/** Database Charset to use in creating database tables. */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/Users/jmbonilla/public_html/jmbonilla/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y+`DcS0.5J3$ 6x5<03v:[0t(NzS8~5;FkMkXsQ!#<5U-[+eWYFNf-A9BRoJ@3I7');
define('SECURE_AUTH_KEY',  '/f[ej1lBRyi+I*cNd7jqfUdQD.@6>7jiF <gnM+m)-3$WPO;okRoT+7@;p>--!|6');
define('LOGGED_IN_KEY',    'Gof.&=/Z-&f*NGvz@=*SoCI}^aR6);Dj o;h%D76*/mo7Rj-dW|d1F4?B!:e)A*b');
define('NONCE_KEY',        '_Xx]F.S-gv~.+r/_|S;RGDZc?t`u~,P^)h*:|6vLj@#Q#i~,=?f=# >#Y)xtl?MM');
define('AUTH_SALT',        '>uhWy&,6JnRu$lC{[&.OWuz,@TjT%m Ei 4kRk<^wg$C:N+t|}DY>$P=.9,/OHf%');
define('SECURE_AUTH_SALT', 'B{Ud0Q5b9M5@qSG-GE)-5|e~N_RjibpL/F,qAk$rATlztGm~IV*6XUd6LJP_p]|C');
define('LOGGED_IN_SALT',   'x&^gZM=&Je}zEP=.sAP^om=(c|KA.A89!MuY$rEk;|R]JNJ%N*qMr+1<sB:afFjD');
define('NONCE_SALT',       'UKpl)Nk+Z/S^seu[ZNDSY^-V)!+C?:8@j7#z|seNRbzO/D-8WOj#i3q[QYZrX$75');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
